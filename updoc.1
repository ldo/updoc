.TH "UPDOC" "1" "2024-01-08" "Geek Central" "A Miscellany Of Useful Commands"
.
.SH NAME
updoc \(em manage versioned snapshots of collections of files
.SH SYNOPSIS
.B updoc [cmd] [args ...]
.SH DESCRIPTION
.PP
.B updoc
is a command-line tool for saving copies of files in an
.UR https://sqlite.org/
SQLite
.UE
database. It implements a range of subcommands for creating a snapshot
database, saving files from a specified directory into it, restoring files from it
into a specified directory, and setting up filter rules.
.
All file paths are saved relative to a specified base directory, and snapshots
are saved using those same paths relative to a specified output base directory.
Since the base directory is always specified as part of the command, this allows
a snapshot to be saved from one directory and restored to a different directory.
.
Note that snapshots do not contain directories as such, only files in those
directories.
.
.SH SUBCOMMANDS
.
The available commands are:
.
.TP
.B help [«cmd»]
with «cmd», displays help for the specified command. If «cmd» is omitted,
shows a list of valid commands.
.
.TP
.B init [\-\-nofilters] «dbname»
initializes a new snapshot database. These are of two types: ones where
the files to be saved are automatically chosen from the specified directory
according to filter rules, or (if
.BR \-\-nofilters
is specified), ones where you explicitly specify the files to be saved in each
snapshot.
.
.TP
.B showfilters «dbname»
shows the filters currently attached to a database that uses filtering.
Reports an error if the database does not use filtering.
.
.TP
.B setfilters «dbname» «filterdefs-filename»
replaces the filters in
.B «dbname»
with those from the given file. The syntax of filter specs is explained in
.B FILTER SYNTAX
below.
.
Reports an error if the database does not use filtering.
.
.TP
.B editfilters [\-\-editor=«editor»] «dbname»
lets you interactively edit the filters attached to a database. The existing
filters are written to a temporary file, which is then opened in the specified
editor. After the command exits, the (possibly updated) contents of this file
are reinstalled as the new filters. You can abort the edit by saving an empty
file. To explicitly delete all filters, you can save a file containing just a
single line beginning with “#” (comment).
.
Reports an error if the database does not use filtering.
.
.TP
.B save [\-\-message=«message»] «dbname» «basedir»
.TP
.B save [\-\-dry\-run] [\-\-message=«message»] «dbname» «basedir» «filespec» ...
saves a snapshot into the specified database
.BR «dbname» ,
taking the files from
.BR «basedir» ,
optionally attaching a descriptive
.BR «message» .
The first form, without any
.B «filespec»
arguments, is used when filtering
is in effect; the filters will be applied to the current contents of
.B «basedir»
to select the files for saving. The second form is used in lieu of filtering,
to explicitly select the files to be saved (all
.BR «filespec» s
must be contained within
.BR «basedir» ).
.
.TP
.B save\-more [\-\-dry\-run] [\-\-existing=«exist\-action»] «dbname» «snapid» «basedir» «filespec» ...
adds more files to an existing snapshot, or replaces existing files in the snapshot.
The given snapshot database must not have filtering in effect.
.
.TP
.B setcomment «dbname» «snapid» «comment»
changes the comment associated with a given snapshot.
.
.TP
.B editcomment [\-\-editor=«editor»] «dbname» «snapid»
lets you interactively edit the comment associated with a given snapshot.
.
.TP
.B unsave [\-\-dry\-run] «dbname» «snapid» «filespec» ...
removes files matching the given
.BR «filespec» s
from the specified snapshot. The
.BR «filespec» s
can contain wildcards to match multiple files; these will need to be escaped
to prevent the shell expanding them.
.
.TP
.B rename «dbname» «snapid» «oldname› «newname»
renames a saved file matching
.B «oldname»
in the snapshot to
.BR «newname» .
.
If
.B «newname»
ends with a slash, then the new name will consist of that
with the part of
.B «oldname»
after the last slash (or the whole of
.BR «oldname» ,
if it contains no slashes) appended to that.
.
.TP
.B list [\-\-long|\-l] «dbname»
lists the snapshots previously saved to database
.B «dbname» ,
including their
assigned snapshot ID, when they were taken and any descriptive message. Specifying
.B \-\-long
(which can be abbreviated to
.BR \-l )
includes details about each file that is part of a snapshot.
.
.TP
.B restore [\-\-dry\-run] [\-\-existing=«exist-action»] [\-\-preserve-timestamps|\-p] «dbname» «snapid» «basedir»
restores the previously-saved snapshot from the database with id
.B «snapid»
into destination directory
.BR «basedir» .
Any necessary subdirectories will be created if any files go into them.
The
.B \-\-preserve\-timestamps
option (which may be abbreviated to
.BR \-p )
indicates whether to restore the original last-modified date/time of each file
as well; if not, it simply gets the current date/time.
.
.TP
.B delete [\-\-verbose|\-v] «dbname» «snapid»
deletes the previously-saved snapshot with the given ID from the snapshot
database. Does nothing if such a snapshot does not exist.
.
.TP
.B copy [\-\-verbose|\-v] «db1name» «snapids» «db2name»
copies one or more snapshots from one snapshot database to another.
.B «snapids»
is a comma-separated list of the snapshot IDs in database
.B «db1name»
to be copied to
.BR «db2name» .
.
.TP
.B copyfilters «db1name» «db2name»
copies the filter rules from database
.B «db1name»
to
database
.BR «db2name» ,
replacing any filters present in the latter. Both databases must have
filtering in effect.
.
.SS COMMON OPTIONS
.
Some options are common across more than one command. These are:
.
.TP
.B \-\-dry\-run
indicates that the operation is only to report what it would do,
without actually doing it; no changes to the database or the
filesystem are made.
.
.TP
.B \-\-existing=«exist-action»
specifies what to do if a file being restored matches the name of one that
already exists. Valid values of
.B «exist-action»
are
.br
*
.B error
\(em Report an error and abort the operation. The default if no
.B «exist-action»
is specified.
.br
*
.B skip
\(em Skip this file.
.br
*
.B replace
\(em Replace the existing file with the new one.
.br
*
.B replace\-all
\(em (for the
.B restore
command only) do the entire restoration into a temporary directory named
.BR «basedir»\-new ,
and when this is complete, the existing
.B «basedir»
and its contents are deleted and
.B «basedir»\-new
is renamed to
.BR «basedir» .
.
.TP
.B \-\-verbose|\-v
.RB ( \-\-verbose
may be abbreviated to
.BR \-v )
specifies whether to report details on what is being done; if omitted, the
operation proceeds quietly.
.
.SH FILTER SYNTAX
.
Filter specifications are given one to a line, in the form
.
.RS 4
.B «keyword» «filespec» [«filespec»...]
.RE
.
Each
.B «filespec»
may contain the usual shell-globbing wildcards “?” and “*”, which are interpreted
when matching them against actual files during snapshot-saving operations.
To prevent special interpretation of these, prefix them with a “\e” character.
Backslash escapes can also be used to include spaces in a
.B «filespec»
as “\e\ ” (to prevent them from being interpreted as token separators), and the “\et” and
“\en” forms can be used to include literal tabs and newlines in a filespec.
.
Filespecs may contain “/” path separators, but may not begin with one; they are
always interpreted relative to some directory.
.
Valid
.BR «keyword» s
are
.
.TP
.B include «filespec» [«filespec»...]
Takes one or more filespec arguments. Include files matching any of the filespecs
relative to the base directory.
.
.TP
.B exclude «filespec» [«filespec»...]
Takes one or more filespec arguments. Exclude files matching any of the filespecss
relative to the base directory.
.
.TP
.B recursive\-include «dirspec» «filespec» [«filespec»...]
Takes at least two arguments, the first being interpreted as a directory spec
relative to the base directory,
and the rest as filespecs. Include files matching any of the filespec arguments
at whatever level within the directory/directories.
.
.TP
.B recursive\-exclude «dirspec» «filespec» [«filespec»...]
Takes at least two arguments, the first being interpreted as a directory spec
relative to the base directory,
and the rest as filespecs. Exclude files matching any of the filespec arguments
at whatever level within the directory/directories.
.
.TP
.B global\-include «filespec» [«filespec»...]
Takes one or more filespec arguments. Include files matching any of the filespecs
wherever they occur in the base directory or in a lower-level directory.
.
.TP
.B global\-exclude «filespec» [«filespec»...]
Takes one or more filespec arguments. Exclude files matching any of the filespecs
wherever they occur in the base directory or in a lower-level directory.
.
.TP
.B graft «dirspec» [«dirspec»...]
Takes one or more arguments which are interpreted as directory specs relative to
the base directory. Include files in any of those directories.
.
.TP
.B prune «dirspec» [«dirspec»...]
Takes one or more arguments which are interpreted as directory specs relative to
the base directory. Exclude files in any of those directories.
.
.PP
.
Rules are applied in the given order to each file found; the last one that applies
takes effect. If no rule applies to a file, it is omitted.
.
When reading filter definitions from a file, lines beginning with “#” are ignored
and can be used for comments. No filter entries are made in the database for such lines.
.
.SH NOTES
.
All database updates are done within transactions. That means, for example, if any
error occurs within a save operation, then the database should not have any partial
save remaining within it. However, the restore operation could end up leaving a
partial restore if, for example, a file already existed and one of the
.B \-\-existing=replace
or
.B \-\-existing=skip
options was not specified.
.
The SQLite binding used is
.UR https://rogerbinns.github.io/apsw/
APSW
.UE ,
not the standard Python library one.
.
.SS WHY NOT VERSION CONTROL?
.
Version-control systems like
.UR https://git-scm.com/
Git
.UE
are popular among software developers. Their strength lies in keeping track of
differences between states of files at different times. Git in particular offers
powerful branching and merging facilities, to facilitate parallel development
of the same project along different paths. For example, the “stable” branch
could be for maintaining the current release, with no new features allowed,
while a separate “testing” branch would be used for developing the next release,
with new enhancements going in on a daily basis, followed by fixes as the bugs
are wrung out of those enhancements.
.PP
But as bugs are found and fixed in the “stable” branch, many of these will likely
also need to be retrofitted to the “testing” branch, which is where merging
comes in. In the process of merging, conflicts might arise where both branches
have made changes to the same source file, which will require hand-editing to fix up.
.PP
Such a process depends crucially on the files concerned being human-readable
.I text
files. Trying to keep track of changes between different versions of
.I binary
files, and worse still, trying to merge changes and fix merge conflicts between
them, is essentially a hopeless task.
.PP
Thus,
.B updoc
simply avoids the whole issue. It concentrates purely on saving versions of files
at particular points in time, no more. It doesn’t try to organize a succession
of snapshots into a “branch”, it has no concept of tracking differences between
successive versions, and naturally the idea of merging different branches doesn’t
even arise. The user can attach an explanatory message to each snapshot, and in
the absence of any file-format-specific tools that might exist for analyzing the data, that is
all the information you have about what has changed in each snapshot.
.PP
There is no parent-child relationship between snapshots, as there is between commits
in Git. For example, in Git, it is not (practically) possible to go back and
change a commit in some way (such as amending the commit message) without having
to regenerate all subsequent commits in the branch. In
.BR updoc ,
on the other hand, prior snapshots can be individually edited and even deleted at any time,
without any impact on later ones.
.
.SH BUGS
.
File contents are loaded in their entirety into memory for saving and restoring. This
probably makes the program unsuitable for dealing with very large files.
.
.SH EXAMPLES
.
.SS EXAMPLE FILTER RULES
.
.PP
.B include *.c
.br
.
Includes all files ending with
.B .c
at the top level of the base directory.
.
.PP
.B recursive\-include src *.c *.py
.br
.
Includes all files ending with
.B .c
or
.B .py
in the
.B src
subdirectory and all directories within it.
.
.PP
.B include src/*.c src/*.py
.br
.
Includes all files ending with
.B .c
or
.B .py
at the top level of the
.B src
subdirectory only.
.
.PP
.B graft src
.br
.
Includes all files within the
.B src
subdirectory down to all levels.
.
.PP
.B prune build-temp
.br
.
Excludes all files in the
.I build-temp
subdirectory down to all levels. (Perhaps it contains temporarily-generated
.B .c
files which are to be excluded from the snapshot.)
.
.PP
.B global\-include *.py
.br
.B global\-exclude _*.py
.br
.B include __init__.py
.br
.
Includes all files with names ending with
.BR .py ,
at all levels within the base directory, including the top-level
.B __init__.py
but
.I excluding
other files with names beginning with an underscore at the top level as well as other
levels. The second rule excludes names
beginning with an underscore from being included by the first rule, and the third rule
puts back the top-level
.B __init__.py
for inclusion.
.
.SS EXAMPLE COMMAND USAGE
.
.PP
.B updoc init test.vers
.br
.
Initializes a snapshot database that uses filter rules named
.BR test.vers .
.
.PP
.B updoc setfilters test.vers filters.txt
.br
.
Sets filter rules into the
.B test.vers
database from the file
.BR filters.txt .
.
.PP
.B updoc save test.vers workdir
.br
.
Saves files from the directory
.B workdir
that match filter rules previously set into the database
.B test.vers
as a new snapshot in that database.
.
.PP
.B updoc list \-l test.vers
.br
.
Gives a long listing of all snapshots saved in the database
.BR test.vers .
.
.PP
.B updoc restore \-\-existing=replace test.vers 1 restoredir
.br
.
Does a restore of the snapshot with ID 1 from
.B test.vers
into the destination directory
.BR restoredir ,
overwriting any existing files with the same names.
.
.SH ENVIRONMENT
.
.TP
.B $EDITOR
used to find the editor command for the
.B editfilters
subcommand, if the
.B \-\-editor
option is not specified.
